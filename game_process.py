from random import randint
from threading import Timer
import pandas as pd
from tkinter import *
import sys


class Game:  # класс отвечает за процесс игры после выбора категории и обработку результатов

    categories = ['dinosaurs', 'geography', 'ghibli', 'history', 'literature', 'cosmos']  # список категорий вопросов

    def __init__(self, category, game_mood, names: list, people: list):
        self.people = people
        self.root = Tk()  # создаем виджет окна
        self.category = category  # и переменные, отвечающие за выбранную категорию, режим игры, имена и текущий счет
        self.game_mood = game_mood
        self.names = names
        self.points1 = 0
        self.points2 = 0
        self.data = pd.read_csv(f'Questions/{category}.csv')  # открываем файл с вопросами выбранной категории
        self.used_questions = []  # список для хранения использованных вопросов
        self.question_number = self.random_questions()  # генерируем случайный вопрос
        if sys.platform != 'linux2':  # увеличиваем окно до размеров экрана
            self.root.wm_state('zoomed')
        else:
            self.root.wm_attributes('-zoomed', True)
        self.root.resizable(False, False)  # запрещаем изменять размеры окна
        self.root.configure(background='#fff6e6')
        self.btn = Button(self.root, text=self.data['var1'].iloc[self.question_number], activebackground='#FAF0E6')
        # создаем кнопки с вариантами
        self.btn.bind('<Button-1>', self.click)  # ответа (4 штуки)
        self.btn.bind('<Configure>', lambda e: self.btn.config(wraplength=500))
        self.btn.place(rely=0.65, relx=0.4, anchor='center', height=120, width=250)
        self.btn2 = Button(self.root, text=self.data['var2'].iloc[self.question_number], activebackground='#FAF0E6')
        self.btn2.bind('<Button-1>', self.click)
        self.btn2.bind('<Configure>', lambda e: self.btn2.config(wraplength=500))
        self.btn2.place(rely=0.65, relx=0.6, anchor='center', height=120, width=250)
        self.btn3 = Button(self.root, text=self.data['var3'].iloc[self.question_number], activebackground='#FAF0E6')
        self.btn3.bind('<Button-1>', self.click)
        self.btn3.bind('<Configure>', lambda e: self.btn3.config(wraplength=500))
        self.btn3.place(rely=0.85, relx=0.6, anchor='center', height=120, width=250)
        self.btn4 = Button(self.root, text=self.data['var4'].iloc[self.question_number], activebackground='#FAF0E6')
        self.btn4.bind('<Button-1>', self.click)
        self.btn4.bind('<Configure>', lambda e: self.btn4.config(wraplength=500))
        self.btn4.place(rely=0.85, relx=0.4, anchor='center', height=120, width=250)
        self.lbl1 = Label(self.root, text=self.data[['questions']].iloc[self.question_number].tolist()[0],  # виджет
                          font=('FreeSans', 14), borderwidth=0, relief='flat', background='#fff6e6')  # с вопросом
        self.lbl1.bind('<Configure>', lambda e: self.lbl1.config(wraplength=500))
        self.lbl1.place(rely=0.3, relx=0.5, anchor='center', height=200)
        self.lbl2 = Label(self.root, text=f'Вопрос № 1', font=('FreeSans', 12),
                          borderwidth=0, relief='flat', background='#fff6e6')  # виджет
        self.lbl2.place(rely=0.1, relx=0.5, anchor='center', height=50, width=600)   # с номером вопроса
        self.lbl3 = Label(self.root, background="#fff6e6", font=('FreeSans', 12), borderwidth=0, relief='flat')
        self.lbl3.place(rely=0.5, relx=0.5, anchor='center', height=50, width=600)
        self.lbl4 = Label(self.root, text=f'{self.names[0]} \n {self.points1}',  # виджет с именем первого игрока и
                          font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')  # его баллами
        self.lbl4.place(rely=0.1, relx=0.15, anchor='center', height=50, width=250)
        if self.game_mood == 2:  # в случае игры вдвоём
            self.lbl4.configure(background='#fff667', font="Verdana 14 bold roman")  # выделяем отвечающего пользователя
            self.lbl5 = Label(self.root, text=f'{self.names[1]} \n {self.points2}',  # добавляем виджет о втором игроке
                              font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
            self.lbl5.place(rely=0.1, relx=0.85, anchor='center', height=50, width=250)
        self.first = True  # определяем, очередь ли первого игрока отвечать
        self.numb = 2  # номер следующего вопроса относительно порядка в раунде
        self.root.title(self.category)  # размещаем в заголовке окна название категории
        self.root.mainloop()  # указываем, что окно приложения не должно закрываться, пока пользователь не сделает этого

    # функция генерирует случайный номер вопроса
    def random_questions(self):
        number = randint(0, 29)
        while number in self.used_questions:
            number = randint(0, 29)
        self.used_questions.append(number)
        return number

    def click(self, event):  # обрабатывает нажатие на кнопку и выводит верно или нет
        answer = event.widget.cget('text')
        if answer == str(self.data[['correct']].iloc[self.question_number].tolist()[0]):
            self.lbl3.configure(text="Верно!", font=('FreeSans', 12), borderwidth=0,
                                relief='flat', background='#fff6e6')
            if self.game_mood == 1:
                self.points1 += 1
                self.lbl4.configure(text=f'{self.names[0]} \n {self.points1}', borderwidth=0, relief='flat',
                                    font=('FreeSans', 12), background='#fff6e6')
            elif self.first:  # при двупользовательском режиме, кроме начисления баллов, меняем активного игрока
                self.points1 += 1
                self.lbl4.configure(text=f'{self.names[0]} \n {self.points1}',
                                    font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
                self.first = False
                self.lbl5.configure(text=f'{self.names[1]} \n {self.points2}', borderwidth=0, relief='flat',
                                    background='#fff667', font=('FreeSans', 12))
            else:
                self.points2 += 1
                self.lbl5.configure(text=f'{self.names[1]} \n {self.points2}',
                                    font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
                self.first = True
                self.lbl4.configure(text=f'{self.names[0]} \n {self.points1}', borderwidth=0, relief='flat',
                                    font=('FreeSans', 12), background='#fff667')
        else:  # в случае неверного ответа только меняем активного игрока
            self.lbl3.configure(text=f'Неверно! Правильный ответ: '
                                     f'{str(self.data[["correct"]].iloc[self.question_number].tolist()[0])}')
            if self.first and self.game_mood == 2:
                self.lbl5.configure(text=f'{self.names[1]} \n {self.points2}', borderwidth=0, relief='flat',
                                    font=('FreeSans', 12), background='#fff667')
                self.first = False
                self.lbl4.configure(text=f'{self.names[0]} \n {self.points1}',
                                    font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
            elif self.game_mood == 2:
                self.lbl5.configure(text=f'{self.names[1]} \n {self.points2}',
                                    font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
                self.first = True
                self.lbl4.configure(text=f'{self.names[0]} \n {self.points1}', borderwidth=0, relief='flat',
                                    font=('FreeSans', 12), background='#fff667')
        t = Timer(2, self.new_question)  # с замедлением переходим к следующей функции
        t.start()

    def new_question(self):  # перезагружает вопросы и варианты ответа
        if self.numb < 11:
            self.question_number = self.random_questions()
            self.lbl1.configure(text=self.data[['questions']].iloc[self.question_number].tolist()[0])
            self.lbl2.configure(text=f"Вопрос №{self.numb}", font=('FreeSans', 12))
            self.lbl3.configure(text="", font=('FreeSans', 12))
            self.btn.configure(text=self.data['var1'].iloc[self.question_number], font=('FreeSans', 10))
            self.btn2.configure(text=self.data['var2'].iloc[self.question_number], font=('FreeSans', 10))
            self.btn3.configure(text=self.data['var3'].iloc[self.question_number], font=('FreeSans', 10))
            self.btn4.configure(text=self.data['var4'].iloc[self.question_number], font=('FreeSans', 10))
            self.numb += 1
        else:
            self.final_window()
            self.root.withdraw()

    def final_window(self):  # выводим результат в разных формах в зависимости от режима игры
        window = Toplevel()
        window.title("Результат")
        window.geometry("800x700")
        x = (window.winfo_screenwidth() - window.winfo_reqwidth()) / 3
        y = (window.winfo_screenheight() - window.winfo_reqheight()) / 12
        window.wm_geometry("+%d+%d" % (x, y))
        window.configure(background='#fff6e6')
        if self.game_mood == 1:
            lbl1 = Label(window, text=f"Отличная игра! Ваш результат: {self.points1} \n Сыграем ещё раз?",
                         font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
        elif self.game_mood == 2 and self.points1 > self.points2:
            lbl1 = Label(window, text=f"Отличная игра! Победу одерживает {self.names[0]} \n Сыграем ещё раз?",
                         font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
        elif self.game_mood == 2 and self.points1 < self.points2:
            lbl1 = Label(window, text=f"Отличная игра! Победу одерживает {self.names[1]} \n Сыграем ещё раз?",
                         font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
        else:
            lbl1 = Label(window, text=f"Отличная игра! Вы сыграли вничью \n Хотите попробовать ещё раз?",
                         font=('FreeSans', 12), borderwidth=0, relief='flat', background='#fff6e6')
        lbl1.place(rely=0.3, relx=0.5, anchor='center', height=200, width=600)
        p = False
        h = False
        if self.game_mood == 1:
            for i in range(len(self.people)):
                if self.people[i][0] < self.points1 and not p:
                    self.people.insert(i, [i+1, self.names[0], self.points1])
                    p = True
            if not p:
                self.people.append([len(self.people), self.names[0], self.points1])
        else:
            for i in range(len(self.people)):
                if self.people[i][0] < self.points1 and not p:
                    self.people.insert(i, [i+1, self.names[0], self.points1])
                    p = True
            for j in range(len(self.people)):
                if self.people[j][0] < self.points2 and not h:
                    self.people.insert(j, [j+1, self.names[1], self.points2])
                    h = True
            if not p:
                self.people.append([len(self.people), self.names[0], self.points1])
            if not h:
                self.people.append([len(self.people), self.names[1], self.points2])
        f = open("leaderboard.txt", "w")
        for x in self.people:
            k = str(str(x[0])+","+str(x[1])+','+str(x[2])+"\n")
            f.write(k)
        btn1 = Button(window, text="Да!", font=('FreeSans', 10), command=lambda:
                      [window.withdraw(), self.__init__(category=self.category, game_mood=self.game_mood,
                                                        names=self.names, people=self.people)])
        btn1.place(rely=0.65, relx=0.3, anchor='center', height=100, width=250)
        btn2 = Button(window, text="Нет", font=('FreeSans', 10), command=lambda: [window.withdraw()])
        btn2.place(rely=0.65, relx=0.7, anchor='center', height=100, width=250)

    def back_to_category(self):
        self.root.destroy()
        self.random_questions()

    def back_to_main_menu(self):
        self.root.destroy()
