import tkinter as tk  # импортируем tkinter
from tkinter import ttk
from tkinter import *
from PIL import ImageTk, Image   # импортируем pillow для добавления изображений png
from tkinter.ttk import *  # импортируем все методы tkinter
from game_process import Game


class Window:

    def __init__(self):
        self.categories = ['dinosaurs', 'geography', 'ghibli', 'history', 'literature', 'cosmos']
        self.root = tk.Tk()  # создаем виджет приложения
        width = self.root.winfo_screenwidth()
        height = self.root.winfo_screenheight()
        self.root.geometry("%dx%d" % (width, height))  # устанавливаем размер виджета на весь экран
        self.root.configure(background='#fff6e6')  # устанавливаем цвет фона
        self.root.title('гризли')  # добавляем название приложения
        icon = tk.PhotoImage(file="b.png")  # ищем изображение для иконки
        self.root.iconphoto(True, icon)  # добавляем иконку приложения

        img1 = Image.open('a.png')  # загружаем изображение1 в приложение
        resized_img1 = img1.resize((400, 100))  # меняем его размеры
        new_img1 = ImageTk.PhotoImage(resized_img1)  # сохраняем измененное изображение
        my_label1 = Label(image=new_img1, borderwidth=0, relief='flat', background='#fff6e6')  # убираем границы вокруг
        my_label1.place(relx=0.5, rely=0.1, anchor='center')  # указываем расположение изображения1

        img2 = Image.open('b.png')  # загружаем изображение2 в приложение
        resized_img2 = img2.resize((400, 300))  # меняем его размеры
        new_img2 = ImageTk.PhotoImage(resized_img2)  # сохраняем измененное изображение
        my_label2 = Label(image=new_img2, borderwidth=0, relief='flat', background='#fff6e6')  # убираем границы вокруг
        my_label2.place(relx=0.5, rely=0.4, anchor='center')  # указываем расположение изображения2
        self.names = []
        self.people = []
        self.game_mood = 1
        f = open("leaderboard.txt").readlines()
        for x in f:
            g = x.split(",")
            self.people.append([int(g[0]), g[1], int(g[2].rstrip("\n"))])
        self.draw_buttons()  # запускаем конструктор кнопок

    def get_names(self):
        self.k = 0  # эта переменная нужна для процесса получения имен при игре 2-х
        self.root_ch = tk.Tk()  # создаем окно для окна выбора количества игроков, темы и дальше
        x = (self.root_ch.winfo_screenwidth() - self.root_ch.winfo_reqwidth()) / 2.5
        y = (self.root_ch.winfo_screenheight() - self.root_ch.winfo_reqheight()) / 4
        self.root_ch.wm_geometry("+%d+%d" % (x, y))
        self.root_ch.geometry("400x400")
        self.root_ch.resizable(False, False)
        self.root_h.withdraw()  # скрываем предыдущее окно
        self.root_ch.configure(background='#fff6e6')  # устанавливаем цвет фона
        self.root_ch.title('гризли')  # добавляем название приложения
        self.label = Label(self.root_ch)
        self.label.place(relx=0.25, rely=0.3)
        self.ent = Entry(self.root_ch)
        self.ent.place(relx=0.35, rely=0.5)
        btn = Button(self.root_ch, text="ввод", command=self.save_name)
        btn.place(relx=0.4, rely=0.7)
        if self.r1.state():
            self.game_mood = 1  # сохраняем режим игры
            self.label.configure(text="Представьтесь, пожалуйста", font=('FreeSans', 11),
                                 borderwidth=0, background='#fff6e6')
        elif self.r2.state():
            self.game_mood = 2  # сохраняем режим игры
            self.label.configure(text="Игрок1, представьтесь, пожалуйста", font=('FreeSans', 11),
                                 borderwidth=0, background='#fff6e6')
        self.root.withdraw()

    def save_name(self):
        self.names.append(self.ent.get())  # пополняем список имен игроков текущей игры
        self.ent.delete(0, tk.END)  # очищаем поле ввода от ответа предыдущего игрока
        if self.game_mood == 1 or self.k == 1:
            self.category()  # запускаем выбор категории
        else:
            self.k += 1  # уточняем, что первый игрок уже представился
            self.label.configure(text="Игрок2, представьтесь, пожалуйста", font=('FreeSans', 12),
                                 borderwidth=0, background='#fff6e6')
        self.root.withdraw()

    def category(self):
        self.root_cat = tk.Tk()  # создаем окно для окна выбора количества игроков, темы и дальше
        x = (self.root_cat.winfo_screenwidth() - self.root_cat.winfo_reqwidth()) / 3
        y = (self.root_cat.winfo_screenheight() - self.root_cat.winfo_reqheight()) / 12
        self.root_cat.wm_geometry("+%d+%d" % (x, y))
        self.root_cat.geometry("700x700")
        self.root_cat.resizable(False, False)
        self.root_ch.withdraw()
        self.root_cat.configure(background='#fff6e6')  # устанавливаем цвет фона
        self.root_cat.title('выбор категории')  # добавляем название приложения
        self.lbl6 = Label(self.root_cat, text="Выберите категорию вопросов", font=('FreeSans', 12),
                          borderwidth=0, background='#fff6e6')
        self.lbl6.place(relx=0.35, rely=0.15)
        btn = Button(self.root_cat, text=self.categories[0], style='W.TButton',
                     command=lambda: [self.root_cat.withdraw(),
                                      Game(self.categories[0], self.game_mood, self.names, self.people)])
        btn.place(rely=0.5, relx=0.2, anchor='center', height=100, width=200)
        btn2 = Button(self.root_cat, text=self.categories[1], style='W.TButton',
                      command=lambda: [self.root_cat.withdraw(),
                                       Game(self.categories[1], self.game_mood, self.names, self.people)])
        # закрываем окно и переходим в окно с вопросами выбранной категории
        btn2.place(rely=0.5, relx=0.5, anchor='center', height=100, width=200)
        btn3 = Button(self.root_cat, text=self.categories[2], style='W.TButton',
                      command=lambda: [self.root_cat.withdraw(),
                                       Game(self.categories[2], self.game_mood, self.names, self.people)])
        btn3.place(rely=0.5, relx=0.8, anchor='center', height=100, width=200)
        btn4 = Button(self.root_cat, text=self.categories[3], style='W.TButton',
                      command=lambda: [self.root_cat.withdraw(),
                                       Game(self.categories[3], self.game_mood, self.names, self.people)])
        btn4.place(rely=0.65, relx=0.2, anchor='center', height=100, width=200)
        btn5 = Button(self.root_cat, text=self.categories[4], style='W.TButton',
                      command=lambda: [self.root_cat.withdraw(),
                                       Game(self.categories[4], self.game_mood, self.names, self.people)])
        btn5.place(rely=0.65, relx=0.5, anchor='center', height=100, width=200)
        btn6 = Button(self.root_cat, text=self.categories[5], style='W.TButton',
                      command=lambda: [self.root_cat.withdraw(),
                                       Game(self.categories[5], self.game_mood, self.names, self.people)])
        btn6.place(rely=0.65, relx=0.8, anchor='center', height=100, width=200)
        self.root.withdraw()

    def start(self):
        self.root_h = tk.Tk()  # создаем окно для окна выбора количества игроков, темы и дальше
        x = (self.root_h.winfo_screenwidth() - self.root_h.winfo_reqwidth()) / 2.5
        y = (self.root_h.winfo_screenheight() - self.root_h.winfo_reqheight()) / 4
        self.root_h.wm_geometry("+%d+%d" % (x, y))
        self.root_h.geometry("400x400")
        self.root_h.resizable(False, False)
        self.root_h.configure(background='#fff6e6')  # устанавливаем цвет фона
        self.root_h.title('гризли')  # добавляем название приложения
        label = Label(self.root_h, text='выберите количество игроков', font=('FreeSans', 12),
                      borderwidth=0, background='#fff6e6')
        label.place(relx=0.25, rely=0.3)
        r_var = IntVar(self.root_h)
        r_var.set(0)
        style = Style(self.root_h)
        style.configure("TRadiobutton", background="#fff6e6", font=("FreeSans", 10))
        self.r1 = Radiobutton(self.root_h, text='1 игрок', variable=r_var, value=0)
        self.r1.place(relx=0.40, rely=0.4)
        self.r2 = Radiobutton(self.root_h, text='2 игрока', variable=r_var, value=1)
        self.r2.place(relx=0.40, rely=0.5)
        btn = Button(self.root_h, text='далее', command=self.get_names, style='W.TButton')
        btn.place(relx=0.40, rely=0.64)
        self.root.withdraw()
        self.root_h.mainloop()
        return

    def help_b(self):
        root_h = tk.Tk()  # создаем окно для помощи в игре
        width_h = self.root.winfo_screenwidth()
        height_h = self.root.winfo_screenheight()
        root_h.geometry("%dx%d" % (width_h, height_h))  # устанавливаем размер окна на весь экран
        root_h.configure(background='#fff6e6')  # устанавливаем цвет фона
        root_h.title('помощь от гризли')  # добавляем название окна
        label = Label(root_h, text='Добро пожаловать в гризли квизли!\n\nДля выбора режима игры, нажмите на кнопку '
                                   '"старт" в главном меню. Вы можете играть в игру одни или же вы можете играть '
                                   'вместе с кем-то на одном устройстве.\nПосле выбора режима игры представьтесь '
                                   'Гризли - введите ваше имя или ник, после чего выберете одну из '
                                   'тематик для вашего квиза:\nдинозавры, география, история, гибли, литература, '
                                   'космос.\n\nВ каждой игре вас ждет по 10 вопросов на игрока. Во время игры вы '
                                   'также можете видеть на экране текущий счет. По окончании игры вы можете '
                                   'продолжить\nигру в выбранном режиме или вернуться в главное меню.\n\nДля '
                                   'просмотра рейтинга игроков, которые представились Гризли вашего устройства, '
                                   'нажмите на кнопку "таблица рекордов" в главном меню.',
                      borderwidth=0, relief='flat', font=('FreeSans', 12), background='#fff6e6')
        label.place(relx=0.5, rely=0.17, anchor="center")
        btn_b = Button(root_h, text='начать играть!',  style='W.TButton',
                       command=lambda: [root_h.destroy(), self.start()])
        btn_b.place(relx=0.40, rely=0.34)
        root_h.mainloop()

    def table(self):
        root_h = tk.Tk()  # создаем окно для помощи в игре
        width_h = root_h.winfo_screenwidth()
        height_h = root_h.winfo_screenheight()
        root_h.geometry("%dx%d" % (width_h, height_h))  # устанавливаем размер окна на весь экран
        root_h.resizable(False, False)
        root_h.configure(background='#fff6e6')  # устанавливаем цвет фона
        root_h.title('рейтинг')  # добавляем название окна
        # определяем столбцы
        root_h.rowconfigure(index=0, weight=1)
        root_h.columnconfigure(index=0, weight=1)
        columns = ("number", "name", "result")
        tree = ttk.Treeview(root_h, columns=columns, show="headings")
        tree.pack(fill=BOTH, expand=1)
        # определяем заголовки с выравниваем по левому краю
        tree.heading("number", text="№", anchor=W)
        tree.heading("name", text="Имя", anchor=W)
        tree.heading("result", text="Результат", anchor=W)
        # настраиваем столбцы
        tree.column("#1", stretch=NO, width=270)
        tree.column("#2", stretch=NO, width=500)
        tree.column("#3", stretch=NO, width=300)
        # добавляем данные
        for person in self.people:
            tree.insert("", END, values=person)
        root_h.mainloop()

    def draw_buttons(self):
        # создаем кнопки
        st = Style()
        st.configure('W.TButton', background='snow2', foreground='black', font=('FreeSans', 11), borderwidth=0)
        button1 = Button(self.root, text='старт', style='W.TButton',
                         command=lambda: [self.start(), self.root.withdraw()])  # кнопка старта
        button2 = Button(self.root, text='таблица рекордов', style='W.TButton',
                         command=lambda: [self.table()])  # кнопка рейтинга
        button3 = Button(self.root, text='помощь', style='W.TButton',
                         command=lambda: [self.help_b()])  # кнопка3 помощи
        # указываем расположение кнопок
        button1.place(relx=0.5, rely=0.2, anchor="center")
        button2.place(relx=0.5, rely=0.61, anchor="center")
        button3.place(relx=0.5, rely=0.67, anchor="center")

        self.root.mainloop()  # указываем, что окно приложения не должно закрываться, пока пользователь не сделает этого
